That is test customer management system applicaion

## Running application

To run applicaion run following commands: 

install dependecies
```
npm install
```
run backend
```
npm run start:backend
```
and in a new terminal session run frontend
```
npm start
```
## Building applicaion

It is possible to build application with
```
npm run build
```
command and then use result static files with some web server like nginx. That is create-react-app functionality 

## Note about create-react-app

I chose create-react-app for that task because it gives all necessary tools for quick start (like testing, eslint, webpack etc). Maybe it lacks some flexibility in configuraion but all defaults seem really reasonable.

## What could be done more or better

There are some obvisous shortages in that implementation. 

For example: 
* Absence of google map api key. That result is warning on the map
* Lack of user design and design responsiveness
* Lack of different kind of tests (I wrote several more for demonstration)

And although that kind of solution looks reasonable for really small project we can imagine a lot of architectural decisions and changes in a case of evolving of this project into something bigger. For example:

* Adding state managment solution (redux, mobx)
* Using flow or TypeScript
* Ejecting create-react-app into scripts and dependencies for full customization or creating infrastructure from a scratch
* Using some error statistics server