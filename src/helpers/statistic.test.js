import { getStatistics } from './statistics'

import * as mathHelpers from './mathHelpers'


// not exactly unit tests, more like integration tests
// minus: some code already was tested by unit tests
// plus: much more useful, because shows how everything works together
describe('statistics', () => {
  const customers = [{
    age: 21,
    balance: '$100',
    longitude: '555',
    latitude: '666'
  }, {
    age: 44,
    balance: '$200'
  }]
  global.fetch = () => Promise.resolve({
    json: () => Promise.resolve(customers)
  })
  it('should transform customers', async () => {
    const stat = await getStatistics()
    expect(stat.customers.length).toEqual(customers.length)
    expect(stat.customers[0]).toEqual({
      age: 21,
      balance: "$100",
      balanceNumber: 100,
      lat: 666,
      lng: 555,
      longitude: '555',
      latitude: '666'
    })
  })

  it('should calculate average age', async () => {
    const stat = await getStatistics()
    expect(stat.averageAge).toEqual(32.5)
  })

  it('should calculate deviation', async () => {
    const stat = await getStatistics()
    expect(stat.deviation).toBeCloseTo(70.71067811865476)
  })
})
