export const getAverage = (arr) =>
  arr.length > 0 ? arr.reduce((sum, next) => sum + next) / arr.length : NaN

export const getDeviation = (arr) => {
  if (arr.length < 2) {
    return NaN
  }
  const average = getAverage(arr)
  const divSum = arr.reduce((sum, next) => sum + Math.pow(next - average, 2), 0)
  return Math.sqrt(divSum / (arr.length - 1))
}