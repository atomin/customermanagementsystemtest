import { getDeviation, getAverage } from './mathHelpers';

describe('mathHelpers', () => {
  describe('deviation', () => {
    it('should calculate deviation', () => {
      const d = getDeviation([1, 7, 4, 100])
      expect(d).toBeCloseTo(48.062459362791664)
    })

    it('for one member array should return NaN', () => {
      const d = getDeviation([1])
      expect(d).toEqual(NaN)
    })

    it('for emtpy should return NaN', () => {
      const d = getDeviation([])
      expect(d).toEqual(NaN)
    })
  })

  describe('average', () => {
    it('should calculate average', () => {
      const d = getAverage([1, 3, 7])
      expect(d).toBeCloseTo(3.66666666666666666666)
    })

    it('average should be NaN for empty array', () => {
      const d = getAverage([])
      expect(d).toEqual(NaN)
    })
  })
})
