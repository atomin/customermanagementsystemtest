import { getAllCustomers } from './backend'
import { getAverage, getDeviation } from './mathHelpers'

export async function getStatistics() {
  const customers = await getAllCustomers()
  const balances = customers.map(c => c.balanceNumber)
  const ages = customers.map(c => c.age)
  return {
    customers,
    averageAge: getAverage(ages),
    deviation: getDeviation(balances)
  }
}