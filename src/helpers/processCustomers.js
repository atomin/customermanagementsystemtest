export default (customers) => customers.map(c => ({
  ...c,
  // '$333,222.11' -> 333222.11
  balanceNumber: Number.parseFloat(c.balance.replace(/\$|,/g, '')),
  lat: Number.parseFloat(c.latitude),
  lng: Number.parseFloat(c.longitude)
}))