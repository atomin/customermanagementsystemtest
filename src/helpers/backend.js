import processCustomers from './processCustomers'

const backend = 'http://localhost:3001'

export const getAllCustomers = () => fetch(`${backend}/customers`)
  .then(x => x.json())
  .then(processCustomers)

export const getCustomersPage = (page, limit) => fetch(`${backend}/customers?_page=${page}&_limit=${limit}`)
  .then(x => x.json())
  .then(processCustomers)

// completely fake implementation
export const getCustomersNumber = () => fetch(`${backend}/customers`)
  .then(x => x.json())
  .then(x => x.length)

export const searchCustomers = (searchText) => fetch(`${backend}/customers?q=${searchText}`)
  .then(x => x.json())
  .then(processCustomers)