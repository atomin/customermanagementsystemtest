import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

import './Menu.css'

class Menu extends Component {
  render() {
    const activeStyle = {
      color: '#00a7a2',
      borderBottom: 'solid #00a7a2'
    }
    return (
      <ul className="nav">
        <li className="nav-item">
          <NavLink className="nav-link" exact activeStyle={activeStyle} to="/">Home</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" exact activeStyle={activeStyle} to="/customers">Customers</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" exact activeStyle={activeStyle} to="/search">Search</NavLink>
        </li>
        <li className="nav-item">
          <NavLink className="nav-link" exact activeStyle={activeStyle} to="/help">Help</NavLink>
        </li>
      </ul>
    )
  }
}

export default Menu
