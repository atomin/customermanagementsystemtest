import React, { Component } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'

import Menu from './Menu'
import Footer from './Footer'
import Routing from './Routing'
import ErrorBoundary from './ErrorBoundary'

import './App.css'

class App extends Component {
  render() {
    return (
      <ErrorBoundary>
        <Router>
          <div className="app">
            <div className="menu">
              <Menu />
            </div>
            <div className="main-content">
              <Routing />
            </div>
            <div className="footer">
              <Footer />
            </div>
          </div>
        </Router>
      </ErrorBoundary>
    )
  }
}

export default App