import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

describe('App component', () => {
  global.fetch = () => Promise.resolve({
    json: () => Promise.resolve([])
  })

  it('deep render without crashing', () => {
    const div = document.createElement('div')
    ReactDOM.render(<App />, div)
    ReactDOM.unmountComponentAtNode(div)
  })
})
