
import React from 'react'
import { shallow } from 'enzyme'

import Statistics from './Statistics'
import * as statisticsHelper from '../helpers/statistics'

describe('Statistics component', () => {
  statisticsHelper.getStatistics = () => Promise.resolve({
    averageAge: 22,
    deviation: 100,
    customers: []
  })

  it('should contain deviation', () => {
    const wrapper = shallow(<Statistics />)
    setImmediate(() => {
      wrapper.update()
      expect(wrapper.contains(<div>Standard deviation: 100.0</div>)).toBeTruthy()
    })
  })

  it('should contain average age', () => {
    const wrapper = shallow(<Statistics />)
    setImmediate(() => {
      wrapper.update()
      expect(wrapper.contains(<div>Average age: 22.0</div>)).toBeTruthy()
    })
  })
})