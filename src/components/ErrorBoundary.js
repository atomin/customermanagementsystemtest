import React, { PureComponent } from 'react'

export default class ErrorBoundary extends PureComponent {
  state = { hasError: false }

  componentDidCatch(error: Error, info: string) {
    this.setState({ hasError: true })
    if (process.env.NODE_ENV === 'development') {
      // eslint-disable-next-line no-console
      console.error(error, info)
    }
  }

  render() {
    if (this.state.hasError) {
      return <h1>Ooops some error happened :(</h1>
    }

    return this.props.children
  }
}
