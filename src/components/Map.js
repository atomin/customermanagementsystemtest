import React, { Component } from 'react'
import PropTypes from 'prop-types'

import './Map.css'

export default class Map extends Component {
  static propTypes = {
    customers: PropTypes.arrayOf(PropTypes.object)
  }

  componentDidMount() {
    this.initMap()
  }

  componentDidUpdate() {
    this.initMap()
  }

  initMap() {
    const { customers = [] } = this.props
    if (customers.length) {
      const map = new window.google.maps.Map(document.getElementById('map'), {
        center: { lat: 0, lng: 0 },
        zoom: 1
      });

      customers
        .filter(({ lat, lng }) => lat != null && lng != null)
        .forEach(({ lat, lng }) =>
          new window.google.maps.Marker({ position: { lat, lng }, map })
        )
    }
  }

  render() {
    return (
      <div id='map'>
      </div>
    )
  }
}
