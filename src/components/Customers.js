import React, { Component } from 'react'

import CustomersTable from './CustomersTable'
import { getCustomersPage, getCustomersNumber } from '../helpers/backend'
import { Button } from 'reactstrap'
import './Customers.css'

const PER_PAGE = 10

export default class Customers extends Component {
  state = { currentPage: 1, customersNumber: 0 }

  componentDidMount() {
    this.loadPage()
    this.setPagesNumber()
  }

  loadPage = async () => {
    const customers = await getCustomersPage(this.state.currentPage, PER_PAGE)
    this.setState({ customers })
  }

  setPagesNumber = async () => {
    const customersNumber = await getCustomersNumber()
    this.setState({ pagesNumber: Math.ceil(customersNumber / PER_PAGE) })
  }

  prevPage = () => {
    if (this.state.currentPage > 1) {
      this.setState({ currentPage: this.state.currentPage - 1 }, this.loadPage)
    }
  }

  nextPage = () => {
    const { currentPage, pagesNumber } = this.state
    if (currentPage < pagesNumber) {
      this.setState({ currentPage: currentPage + 1 }, this.loadPage)
    }
  }

  render() {
    const { customers, currentPage, pagesNumber } = this.state
    return (
      <div className='customers-table-with-controls'>
        <div className='customers-table-container'>
          <CustomersTable customers={customers} />
        </div>
        <div className='page-controls'>
          <Button color="info" onClick={this.prevPage}>
            Prev Page
          </Button>
          <span className='current-page'>{currentPage} from {pagesNumber}</span>
          <Button color="info" onClick={this.nextPage}>
            Next Page
          </Button>
        </div>
      </div>
    )
  }
}
