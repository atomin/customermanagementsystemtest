import React, { Component, Fragment } from 'react'

import { getStatistics } from '../helpers/statistics'
import Map from './Map'

class Statistics extends Component {
  state = {
    averageAge: 0,
    deviation: 0,
    customers: []
  }
  async componentDidMount() {
    const statistics = await getStatistics()
    this.setState(statistics)
  }
  render() {
    const { averageAge, deviation, customers } = this.state
    // todo more meaningful markup
    return (
      <Fragment>
        <div>
          Customers count: {customers.length}
        </div>
        <div>
          Average age: {averageAge.toFixed(1)}
        </div>
        <div>
          Standard deviation: {deviation.toFixed(1)}
        </div>
        <Map customers={customers} />
      </Fragment>
    )
  }
}

export default Statistics
