import React, { Component } from 'react'
import debounce from 'lodash/debounce'

import CustomersTable from './CustomersTable'
import { searchCustomers } from '../helpers/backend'
import { Input } from 'reactstrap'
import './Search.css'

const SEARCH_MIN_CHARS = 3

class Search extends Component {
  state = { customers: [] }

  search = async (text = '') => {
    if (text.length >= SEARCH_MIN_CHARS) {
      const customers = await searchCustomers(text)
      this.setState({ customers })
    } else {
      this.setState({ customers: [] })
    }
  }

  searchDebounced = debounce(this.search, 500)

  searchInputChange = (event) => {
    this.searchDebounced(event.target.value)
  }

  render() {
    return (
      <div>
        <div className='search-input-container'>
          <Input type='text'
            onChange={this.searchInputChange}
            placeholder='Search customers...'
            className='search-input'
          />
        </div>
        <CustomersTable customers={this.state.customers} />
      </div>
    )
  }
}

export default Search
