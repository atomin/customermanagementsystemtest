import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Table } from 'reactstrap'
import './CustomersTable.css'

export default class CustomersTable extends Component {
  static propTypes = {
    customers: PropTypes.arrayOf(PropTypes.object)
  }

  render() {
    const { customers = [] } = this.props

    return (
      <div className='customers-table'>
        {customers.length > 0 && (
          <Table striped>
            <thead>
              <tr>
                <th>Name</th>
                <th>Age</th>
                <th>Balance</th>
                <th>Phone number</th>
              </tr>
            </thead>
            <tbody>
              {customers.map(({ name: { last, first }, age, balance, phone, _id }) =>
                <tr key={_id}>
                  <td>{`${first} ${last}`}</td>
                  <td>{age}</td>
                  <td>{balance}</td>
                  <td>{phone}</td>
                </tr>
              )}
            </tbody>
          </Table>
        )}
      </div>
    )
  }
}
