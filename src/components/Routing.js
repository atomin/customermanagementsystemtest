// @flow
import React, { Component, Fragment } from 'react'
import { Route } from 'react-router-dom'
import Customers from './Customers'
import Statistics from './Statistics'
import Search from './Search'
import Help from './Help'

export default class Routing extends Component {
  render() {
    return (
      <Fragment>
        <Route path="/customers" component={Customers} />
        <Route path="/search" component={Search} />
        <Route path="/help" component={Help} />
        <Route exact path="/" component={Statistics} />
      </Fragment>
    )
  }
}
